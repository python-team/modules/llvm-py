llvm-py (0.6+svn105-3) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:00:22 -0500

llvm-py (0.6+svn105-2) unstable; urgency=low

  * debian/control
    - Bump B-D llvm-2.8 and llvm-2.8-dev to 2.9
    - Upgrade Standards-Version to 3.9.3
    - Removed ${python:Breaks}
  * debian/patches
    - Unlink-static-lib.patch  for llvm 2.9
    - Add update-2-9.patch for llvm-py to support llvm 2.9
      (Closes: #662880)
  * debian/rules
    - Added -r 105 to svn checkout for get-orig-source

 -- Carl Chenet <chaica@ohmytux.com>  Sun, 11 Mar 2012 12:35:23 +0100

llvm-py (0.6+svn105-1) unstable; urgency=low

  * Version from the upstream SVN to support llvm 2.8 (Closes: #624901)
  * debian/control
    - Removed python-support
    - Bump python, python-all-dev, python-all-dbg to 2.6.6-3 in Build-Depends
    - Bump Standards-Version to 3.9.2
    - Removed Breaks: ${python:Breaks}
    - Using llvm-2.8 and llvm-2.8-dev in Build-Depends
    - Replace XS-Python-Version by X-Python-Version
  * debian/copyright
    - Added new authors/copyrights for new files in the upstream
      sources.
  * debian/rules
    - Replaced dh $@ by dh $@ --with python2
    - Modified get-orig-source section
  * debian/patches
    - Added unlink-static-lib.patch (Closes: #597835)
  * debian/source/format
    - Bump to 3.0 (quilt)

 -- Carl Chenet <chaica@ohmytux.com>  Fri, 20 May 2011 04:55:19 +0200

llvm-py (0.5+svn85-1) unstable; urgency=low

  * Initial release. (Closes: #552132)

 -- Carl Chenet <chaica@ohmytux.com>  Tue, 08 Mar 2011 00:14:57 +0100
